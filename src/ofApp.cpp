#include "ofApp.h"


//--------------------------------------------------------------
void ofApp::setup(){
    ofEnableAntiAliasing();
    ofEnableSmoothing();
    ofSetCircleResolution(100); //how many sides for ellispes?
    xRect = 0; //starting value for the rectangle position
    myCircColor.setHsb(0, 255, 255); //starting color as HSB
}

//--------------------------------------------------------------
void ofApp::update(){
    xRect+=1.4; //move the rectangle by 1.4 pixels
    if(xRect>ofGetWidth()) xRect = 0; //reset the rectangle position if it goes off the right side
}

//--------------------------------------------------------------
void ofApp::draw(){
    ofBackground(220, 120, 110); //make a fresh background
    ofSetColor(255); //set color to white
    
    float numCirc = ofGetMouseX()/8; //set the number of circles to go from 0-99, depending on the mouse X position
    cout<<"number of circles is "<<numCirc<<endl; //print to console
    float radius = ofGetMouseY()/3; //the radius of the circle of circles goes from 0-200
    
    //This for loop shows how to position the circles with radial to cartesian math
//    for(int i = 0;i<numCirc;i++){
//        int x = sin(i/numCirc*TWO_PI)*radius;
//        int y = cos(i/numCirc*TWO_PI)*radius;
//        ofSetColor(i*5);
//        ofDrawEllipse(x+400, y+300, 50, 50);
//    }
    
    //here we make a ring of ciurcles by using transform matrices
    ofPushMatrix(); //we will center everything inside this matrix
    ofTranslate(ofGetWidth()/2,ofGetHeight()/2); //this translates everything to the middle
    for(int i = 0;i<numCirc;i++){ //make a number of circles in a ring
        ofPushMatrix(); //this matrix allows us to move and rotate the small circles
        ofRotateDeg(360/numCirc*i); //spin each one around a portion of the ring
        ofTranslate(0, radius); //shift the circle offcenter by the desired radius
        myCircColor.setHsb(i*255/numCirc, 255, 255);//use the entire range of hue for the number of circles in the rung
        ofSetColor(myCircColor);
        ofDrawEllipse(0, 0, 50, 50); //draw the ellipse at 0,0
        ofPopMatrix(); //stop the transformations
    }

    
    //here we use sin and cos to make a curvy star with a polyLine
    float angle = 0;
    while (angle < TWO_PI ) { //while is a simpler form of a for loop
        b.curveTo(100*cos(angle), 100*sin(angle)); //interior points
        b.curveTo(300*cos(angle), 300*sin(angle)); //arms of the star
        angle += TWO_PI / 10; //make 10 arms
    }
    b.draw(); //draw the polyLine
    ofPopMatrix(); //stop the transformation that was centering everything
    
    ofSetColor(100, 200, 255);
    ofDrawRectRounded(xRect, 500, 200, 100, 20);
}
